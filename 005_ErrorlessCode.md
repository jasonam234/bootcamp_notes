# Day 5 - Errorless Code

Ada beberapa scenario yang tidak terpikirkan dan jadinya aplikasi jadi error

Anggap koneksi ke database / SMTP / LibrarySMS mahal

Buat aplikasi yang test-driven app

Koding yang error juga bisa karena workmate sendiri, misal gitnya tabrakan. 

## Exceptions Handling

Try catch untuk menangkap error jadi tidak crash. Exception error yang terdeteksi ketika runtime

Misal 

```
int a
int b
c = a / b
```

Kemungkinan ArithmeticException, division by zero.

Gunakan try catch agar app tidak crash & return error code.



Pake if juga memungkinkan.

Untuk errorless code dengan input string, bisa pake Regex



Read Exception documentation



Null Pointer Exception sering ditemukan, misal ketika panggil method dari class lain. Ketika panggil object tersebut, dan belum di initiate & data null, akan NPE. Semua object bisa Null.

NPE Terjadi ketika panggil variable yang menunjuk nilai null

NPE Juga terjadi kalau data incompete.s MIsal ada halaman product & punya gambar semua, tapi 1 gambar tidak ada. NPE

Cek string null bisa menggunakan ternary operator, if-else



### Hierarchy of Exceptions

printStackTrace() -> menampilkan semua error yang terjadi, tapi kadang teralu banyak.

checked-exception sudah dijagain oleh java lib sendiri



1. **Checked Exception**

   FileNotFoundException() -> file thidak ditemukan.

   Bisa pake try-catch

   bisa pake throw

   Ketika menggunakan try-catch akan handle error di class itu sendiri

   Perbedaan ada di mau handle error dimana

2. **Unchecked-exception**

   Bisa dimana saja. 



Tidak diminta untuk bungkus semua code dengan exception.



Array bisa rentan dengan OutOfBoundsException, jadi ketika ada input / akses array, pake if-else check indexnya outofbound / minus



Exception Handling menggunakan try-catch statement

try isinya code yang ingin di check

catch isinya exception

finally jalanin kode apapun keadannya



Ketika catch semua exception, aplikasi bisa tidak efisien. 

Bisa multiple catching

```catach
try{
	c = 2/0;
	d -> null;
	//wajib berurutan
}catch(ArithemticException e){

}catch(NPE e){

}catch(SQLException | IOException){
	bisa di pipeline
}
```

Tapi 1 exception yang menangkap semua exception cukup

catch akan di exception **kalau** ada error



finally dipakai ketika menggunkana koneksi ke external, misal dari app ke database. Finally biasanya buat close dbConnection.



Bisa juga try langsung finally

```
try{

}finally{

}
```

Tapi sedikit berbahaya, karena bisa exception bisa ada yang tertangkap



Method lain bisa throw exception ke method main.



Try with resources

```
try(Reader reader1 = new FileReader("aaa.txt") ; Database database1 = new Database(dbConn)){

}
```



### Throwing exception

Jika ada throw di method, yang harus handle errornya ada di main methodnya. Error di lempar ke man method. Bisa multiple exception. 

Bisa juga custom exception



## Logging

Lebih logging ke error

Jika error terjadi minimal bisa di baca / tau kapan error terjadi

Pake lib java.util.logging

Ada banyak level logger.



Sebagai alat bantu biar error lebih readable.



### Handson

Buat app vending machine, setiap kolom ada index, setiap row juga ada index (membentuk koordinat).

Tugasnya Buat 6 baris. 

Uang diperhatikan. Kembalian dan uang kurang





## Testing tools & library

Create test scenario untuk case yang repetitive. 

Unit testing ada framework Junit & Mockito.

Jangan lupa unit testing ketika membuat app. Buat code yang test driven code. Ketika buat app minimal buat 4-5 cornercasing dari feature yang dibuat, misal login password:

- Password salah
- Password kosong
- Password sukses
- etc



Kasih tag @Test

```
@Test
public void testMethod(){
	...
}
```



bisa method untuk persiapan dari case scenario masing-masing

@Before, @BeforeClass, @After & @AfterClass



Assertion

assertEquals

assertTrue

assertNotNull



Ketika dibuat feature baru dibuat pasti akan nyengol method / variable lain.



### Mockito

Ketika punya app yang connect ke database, pake mockito, "data" sudah disediakan oleh mockito



Ketika test minimal menggunakan 3 assertion. 

Fokus ke feature yang paling baru. 



Urutan Test:

1. @BeforeAll -> Initialize data, bisa set expected outcome juga
2. Test class instance const
3. @BeforeEach -> Declare variable yang banyak dipakai, misal password, username dan object user
4. @Test -> test case masing-masing, apa yang mau di test ditulis di bawah @Test
5. @AfterEach -> Hasil dari masing-masing testcase

if Done

6. @AfterAll -> delete semua data sisa hasi testing





Test Driven model : buat test class dulu, baru ke model.



Jangan lupa test jika variable yang diterima null / empty



Biasakan ketika buat class buat testclassnya juga

ketika buat method baru buat testmethodnya juga



### Formative

