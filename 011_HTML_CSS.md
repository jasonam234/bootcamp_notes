# 011

### Tag 

```
<p></p>
<h1></h1>
etc
```

 

### Attribute

```
<a href="google.com"> </a>
```

href attribute dari tag a



### Tag head

untuk taro tag meta, meta biasanya untuk SEO, untuk indexing ke search engine. 

charset buat bahasanya

viewport untuk atur scale responsive

title buat judul di tab google chrome

Buat taro link css 



### CSS

Disarankan pake framework css, biar lebih mudah & hemat waktu

kalau butuh class baru, nama class baru jangan sama dengan nama class dari framework

Kalau lebih dari 2 style, mending buatclass sendiri & external



```
.warna-ijo{
	color : greenyellow !important;
}
```

!important -> force output 



Prioritas

0. Class with !important

1. ID #
2. Style inline
3. Class .



### Parent-child

```
<div class="warna-ijo">
	<p>Ini Paragraf</p>
	<h1>Ini Heading</h1>
</div>
```

Style dari child bakal refer ke parentnya. Tapi child bisa punya style sendiri tinggal tambahain di tag childnya. 



Frontend banyakin pake CSS framework.

Frontend Urusin data dari backend, button click, logic etc. Biar halaman lebih dynamic

Web Design HTML CSS custom 



### Tugas 

3 -> pake bulma css framework

Buat website e-comm Beauty & care

Minimal 2 halaman. Harus ada form inputan, misal mau beli, arahin ke form. **Desain saja**.

