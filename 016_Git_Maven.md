# 016 Git & Maven

Dependency injection baca dulu.

Setiap pekerjaan harus di commit, meskipun belum selesai. Step by step dari pekerjaan di commit. Karena biasanya jam commit / konten yang dibuat bakal dinilai. Git history bakal dinilai

keyword **origin** refer ke master yang ada di repo online. 

```
git init -> git add -> git commit -> git push -> A
A -> (git add  -> git commit -> git pull -> git push)Repeat
```

Harus **pull** dulu sebelum push, pastikan gk ada error / conflict.

Secara urutan yang paling bener dari online repo ke local repo.

Default text editor Eclipse.

## Versioning

V1.0.0

digit 3 buat fitur kecilnya [patch_version]

digit 2 buat buat fitur besarnya [minor]

digit 1 buat aplikasi yang bisa dipublish [major]

Tergantung dari team sendiri juga.

## Git

**Git manual, hampir semua command ada di git**

```
git help
```

**Inisialisasi project dari local, scopenya masih local**

```
git init
```

Atur config di git

```
git config 
git config --global user.name "jasonam234"
git config --global user.email "email@gmail.com"
```

**Tambahin file yang sudah di add**

```
git add directory/filename
git add .
git add *
git add --all
git add *.txt
git add docs/.txt
```

git add * / git add . buat tambahin semua file yang ada di dir.

**Git status**

```
git status -s
A filename.txt
```

**Git Commits**

```
git commit -m "Initial commit"
```

Usahakan message / comment sedeskriptif mungkin, singkat padat jelas. Misal

```
git commit -m "[feature-name] - Features_added"
git commit -m "[Login Feature] - Added JWT Token to sign up"
```

**History git**

```
git log
```

**See all commit differences**

```
git diff
```

**Delete all commits history**

```
git reset [] [] 
git reset -soft HEAD^ //yang di reset dikumpulin ke 1 folder
```

**Check if local git connected to online repo**

```
git remote
```

**Connect to online repo from local repo**

```
git remote add origin [repo_link]
```

Harus ada repo online dulu.

Semua branch yang dibuat bakal di push ke online repo.

**Removing remote from local repo**

```
git remove rm
```

**Pushing to local branch**

```
git push -u origin master
```

Sync antara local repo & online repo

**Clone project from online git**

```
git clone [repo_link]
```

**Checking branch on repo**

```
git branch
```

**Creating branch on repo**

```
git branch [branch_name]
```

**Change branch**

```
git checkout [branch_name]
```

**Merging**

```
git merge [branch_name]
```

Gabungin branch [branch_name] dengan branch yang dipilih sekarang.

Ambil semua yang ada di [branch_name] ke semua yang ada di branch yang dipilih sekarang. 

Biasanya untuk branch branch local saja. 

**Deleting a branch**

```
git branch -d [branch_name] //normal delete
git branch -D [branch_name] //force delete without commit
```

**Tagging**

```
git checkout v0.0.1
```

Bisa juga lewat UI di gitlab 

**Ambil perubahan dari git online**

````
git fetch
````



## Maven

Di js semacam node modules, untuk manage dependencies dari project.

Project management tools. Nulis dependencies di project biar ketika di clone, user lain sisa download package / dependencies yang ditulis. 

JUNIT, Mockito masuk ke dependencies Maven. Kalau misal butuh Junit, harus ditulis di POM-nya.

Dependencies itu aplikasi yang dibuat orang lain yang bisa digunain di project. anggepannya module dari org lain.

Artifact = nama project ketika di init pertama kali

vonage.client => nama artifact dari vonage



### Starting MVN project

1. start.spring.io, masukkin data & extract ke folder default
2. Import di eclipse, pilih maven, pilih existing maven project.
3. Arahin ke directory project, POM bakal di generate otomatis



### Adding new dependencies to MVN project

1. mvnrepository.com
2. search dependencies yang mau dicari,
3. Misal vonage, pilih vonage, copy dependencies sesuai dengan build tools.
4. pindahin ke pom.xml
5. import di Applikcation.java, caranya:
   1. Ketik nama depan dependencies, 1/2 dari nama
   2. ctrl+space di nama, klik yang sesuai
   3. Auto-generate dependencies.



## Formative

misal 10 anggota , per orang 150.000

yang menang gk boleh menang lagi. 

Unit testing ditambahin ?

Masing-masing peserta punya random code. Undian berdasarkan random code. 

Setiap pengocokan, random code bakal berubah. 

Buat lagi mavennya & spring

console based

Anggota boleh tambah / fix.



