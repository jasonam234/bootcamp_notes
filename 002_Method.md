# Day 002 Code Style - Method - OOP

## Code Style

perlu ada code style biar readable

### Comment

EOL comment

Jgn teralu banyak, singkat padat jelas. Kalau bisa di belakang line

````
 //Text Here
````

MultiLIne Comment

Biasanya disimpan diatas class /method untuk menjelaskan. Method getname tidak perlu, calculasi kompleks mungkin perlu. **Max 3 line (best practice)**

```
 /* 
 Text here
 */
```

Java document comments 

Annotation, ada @param, @return. Nanti akan didapatkan javadocnya. 

```
/*
 *
 * bla bla bla
 *
 * @param args from the command line
 *
 */
```



Comment seperlunya saja, jgn per line of code di comment. Pastikan comment ada artinya juga, jgn untuk asal pass test. 



Pemilihan kata kata di sebuah code juga penting. 

Code convention yang digunakan OCC (Oracle Code Conventions) :

- Indent tab 4 space, pastikan indent tab sama semua
- Curly Braces di belakang statement / nama method. Hemat scroll terutama scroll horizontal
- Extra space dihilangkan
- Panjang code dalam 1 baris 80 karakter maximal. **Hemat scrollbar !!**. Batas lainnay 100 120 / 140.  



### Coding Style Variable

Nama variable harus representatif deskriptif & singkat. Singakatan boleh. for loop int i masih diterima.

Rules :

- Case sensitive
- Unicode
- Tidak boleh dimulai oleh angkat
- tidak boleh pakai reserved keyword dari java
- space tidak boleh
- satu kata harus huruf kecil
- camelCase
- jangan memulai dengan _ atau $
- pilih nama yang paling cocok



## Method

Lakuin 1 hal yang spesifik yang di defined

example

```
getVolume(a ,b, h); //panggil method
```

Nama method mencerminkan penggunaan method

Pastikan ketika buat aplikasi buat perintilan perintilan kecilnya

Penamaan method camelCase & merepresentasikan kegunaan Function



Ada beberapa method static yang bisa dipanggil tanpa harus menginisasi obejct, utility method. 

Non-static method harus dipanggil dulu objectnya. 



Nama parameter yang dipakai harus representatif juga. Harus full kata dan jangan singkatan.



Return type void bisa pake return, asal returnnya NULL



Overloading bentuk dari static polymorphism, 1 method yang bernama sama bisa banyak bentuk



### Overloading & Casting

Implicit Casting, method yang di overload akan mengikuti variable type berdasarkan prioritas implicit casting. 

Casting : paksa berubah nilai variable, short harus explicit (diketik) agar bisa di cast

long jarang dipake karena tidak ada limit memory. Biar aman pake int float, double



### Main method

Java wajib ada 1 class dan 1 method, biasnaya main method. 

Diakses sebagai entrypoint dari java app



### Functional Decomposition

Menguraikan masalah menjadi subproblem

Bagian bagian kecil program dipecah jadi beberapa method. 



Recursion yang paling sering ditemukan di dunia nyata kategori.

Misal

```
ID | Name        | Parent
1  | TV          | -1
2  | LED TV      | 1
3  | LED TV 32"  | 2
4  | LED TV 33"  | 2
5  | LED TV 34"  | 2

```

Tablenya recursive.



### Handson

Buat aplikasi Liburan. di model liburan ada 3 model tokyo, newyork, banding. di tokyo ada tempat wisata, newyork dan bandung juga sama. Ada harga per tujuannya juga. 

Menghitung dana liburan. 



### Kerja Kelompok

buat app airplane ada beberapa class, harga ekonomi 100%, jadi standartnya ekonomi, ada 3 type pesawat sesuai dengan requirement

Misal'

pilihan awal destinasi. harga yang tertera harga ekonomi. 

terus ekonomi / business / first class

luggage 



## OOP

fundamental of Java

creating object

```
Pation john = new Patient();

//Behaviour / character of object
john.name();
john.age();
```



Constructor untuk menginisialisasi object baru di class, syarat

- nama sama dengan nama kelass
- tidak punya return type



ada paramterised const

```
public Patient(String name, int name, float height){
	this.name=name;
	etc
	etc
}

main{
	Patieng john = Patient(apa, apa, apa); //bisa langsung dapet value
}
```



bisa overload const, refer ke slide sebelumnya



kata this() untuk panggil constructor sendiri. 

Constuctor / settergetter tergantung coding. 



### Class

Static bisa dipanggil langsung

Instance method harus dipanggil kembali, tapi bisa akses variable di dalam class

jika pakai static pastikan final. Biasa static digunakan jadi konstanta



```
public static String variableName;
```

bahayanya karena asumsi menggunakan static nilai tidak berubah

konstanta gunakan huruf besar.



### Packaging

Folder Structure untuk memisahkan file agar lebih terbaca / dikelompokkan

Biasanya packaging model collection utils. Di nexsoft 4 controller, service, model, repositoryf

access modifier, jadi lebih aman 



cara import 

```
import org.company.java.packages.theory.p3.[nama_class]
```

Best practice, import seperlunya, biar load tidak berat



### Access Modifier

untuk kejelasan dan keamanan code



Jika ingin membuat class dari package yang berbeda -> private with getter setter

Jika tidak -> private atau protected





### Getter & Setter

prinsip encapsulation agar tidak bisa diakses langsung. Bisa read only / write only

CornerCasing, gimana jika setname tapi null. 



### Object

Immutable

Nullable -> bisa dijadikan null



Boxing & Unboxing

Autoboxing -> auto convert oleh java



Boxing -> native ke object

Unboxing -> Object ke Native



Null pointer exception, jangan lupa cornercasing



### Class Hierarchies

extends to parent class

Pake kata extends hanya bisa 1x

Parent bisa punya banyak child



Best practice jangan langsung buka IDE & Koding

Buat structure classnya terlebih dahulu

1-3 hari gambar diagram

Pastikan Skema jalan



toString() -> print info error, tampilin message spesifik

Bisa di @Override untuk print info yang lebih spesifik



@Override untuk define method di subclass



Abstract -> absolute blueprint untuk class



### Interface

Interfacce bisa extend ke interface lainnya

Interface digunakan untuk loose coupling

Bisa di reuse untuk hal hal lainnya



### Annontations

metadata untuk memberi info program

untuk beri info ke compiler / ke developer

Misal

```
@Deprecated : mau dihapus
@SuprressWarning : warning dihilangkan, messagenya gk kena exception
@Override : untuk override method
@NotNull : variable tidak boleh null / tidak seharusnya mengembalikan NULL
@Range : mengembalikan bilbul
@Retention : ...
@Target

@param
@return
```

Read Documentation



Inner class jangan digunakan, karena sudah ada packaging



### Enumeration

Predefined variable, sudah ditentukan sebelumnya. Tidak akan mendapatkan pilihan dari enumeration yanag ditentukan



Buat aplikasi yang moduler / bisa berkembang



### Formative

ada class bioskop, ada class studio, emp dll

ada setter getter / bebas

Deadline jam 9 pagi besok



