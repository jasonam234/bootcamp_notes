# Day 004 - JVM & Reflection

## Inside JVM

Compilation : dari java ke native code

Pastikan ketika buat app, versi dari java / programming language sama yg ada di production.



JDK : Java Dev Kit, yg paling lengkap toolsnya. Ada archiver (..., ..., JAR)

JRE : Java Runtime Env, buat jalanin app

JCL : library I/O di java, banyak toolkit



### Call Stack

Misal ada sebuah method / class, yang ada logicnya. method dipanggil dan return nilai yang ada. 

Setiap method akan dipecah jadi call stack di java.

Nested method bisa jadi contoh.

```
public calculate(int a, int b){
	logic 
	call method
	logic
	return c
}

Stack

return c
logic
call method
logic paling atas
method name
Param (masuk pertama)
```

Setelah selesai stack akan di pop

Ketika stack kosong execution berhenti



Stackoverflow JUmlah method yang dipanggil teralu banyak & overflowing. Akan ada error StackOverflowError



Best Practice, ambil memory kecil dulu, kalau kurang baru tambah. Kalau ambil memory teralu banyak akan redundant dan jadi tidak efisien.



```
java -cp . Main
```

. -> current working dir



```
javap -c Main.class
```

Dari class file ke .java



Kalau menjalankan app harus hasil akhirnya saja. kalau orang lain akses .java bisa di crack 



Garbage Collector 

Buang sesuatu di memory yang sudah tidak diperlukan.



Tools untuk analysis java

Eclipse -> MAT (Memory Analyze)

VisualVM

dumpFile -> Snapshot status memory, kalau tidak bisa lihat secara realtime



Aplikasi yang sehat / bagus memorynya fluctuatif dan stabil. Kalau ada penurunan yang drastis berarti garbage collector sedang berjalan. 



```
System.gc()
```

Call garbage collector in code, tapi garbage collector jalan pas dibutuhkan saja, tergantung dari dev / jvm

Tidak wajib untuk menggunakan garbage collector. 

Tidak disarankan untuk panggil GC secara manual. JVM yang kerja. 



## Java Reflection

Akses dan modif aplikasi pada saat runtime.

const, fields dan method bisa berubah di runtime



Reflection :

- Field : mendapatkan dan mengubah nama nilai, data type dan access modifier
- Method : bisa ubah method



Menampilkan semua info yang ada di class lain. Berguna buat thirdparty framework tanpa documentation



Yang paling sering digunakan Class.forName();



Di real case, ada kemungkinan / tidak ada sama sekali jadi devops. Ketika jadi devops lebih sering menggunakan Reflection. Tapi di real case jarang dipakai.



lihat bayangan dari class / app yang tidak diketahui isinya. 



Jmeter Untuk design load test app java. 

Jmeter + visualVM untuk simulasi real world

Selenium



### Formative

fitur contoh contacts, chat

yang di contoh baru 1 level

Penjelasan di 1 txt terpisah. 



Minimal 5 dari 10 punya feature 4 level