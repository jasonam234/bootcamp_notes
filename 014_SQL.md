# 014 - Git & SQL

Javascript pisah per function, biar bisa lebih dibaca. 

### Git

Masing masing anggota buat branch sendiri.

Nama branchnya bebas, tapi sesuain dengan nama fiturnya. 

Tiap buat repo baru. buat branch master & development. 

Master kosong, development app yang paling update (?)

Ketika cloning, clone ke development. 

Misal ada fitur login, buat branch login & buat fitur disitu. Ketika branch login udh di merge, delete branch login.

Misal upload assets, buat branch global configuration

di gitlab main == master



### Branching

Dari main / master buat branch misalnya development. Biar main / master tetep kosong. Readme.md boleh ada di main / master.

Branch main / master cuman admin yang boleh edit. Branch development buat "Mainnya". Masing-masing anggotanya buat branch baru.

Buat branch di dalam branch juga bisa. 

Anggota dari repo tidak dizinin buat push ke development langsung, harus ada request (?)

Perhatikan git, sebelum di push gk bakal berpengaruh ke gitlabnya langsung

Ada proses merging data antara branch development & branch masing-masing



**Clone from spesific branch**

harus dibuat dulu branchnya

Kalau di local buat branch development, push. Trus merge request

```
git clone -b [branch name] [url_https]
```

Atau

```
git clone [url_https] --branch [branch_name]
```

langsung clone dari branch [branch name]



**Cek nama branch**

```
git branch
```



**Create new branch & go to branch**

```
git branch [branch_name]
git checkout [branch_name]
git branch //check current branch
git pull origin [branch_name] // check if there is any update(s)
```

Nama branch sesuai dengan ketentuan group. Misal

```
git branch [feature_name]
git branch [[dev_name]/[feature_name]]
```



## **Push newest version to repo**

**Cek nama file yang belom ke track**

```
git status -s 
```

**Add ke git local ?**

```
git add .
[Atau]
git add *
git status // file yang belum ke track jadi ke track
```

**Commit git**

```
git commit -m "[descriptive_comment]"
```

Biasanya :

```
git commit -m "[Login Feature] - Added new login feature with JWT"
```

Maksud dari commit harus jelasin di commentnya

**Pull from repo & push the newest file**

```
git pull origin development
```

Pastikan coding sudah paling update. Cek if there is a conflict. 

Kalau tidak ada conflict bisa lanjut. 

```
git push origin login
```

Abis push ke repo, bisa create merge request. Merge request ada di gitlab. Title auto isi.

Assignees -> orang yang approval. 

Reviewers -> yang review, bebas pilih siapa aja. Bisa bantu lead 

**keyword origin**

```
git pull development -> local
git pull origin development -> langsung pull ke repo
```

Perhatikan merge options. Pilih option **"Delete source branch when merge request is accepted"**

Langsung create merge request di gitlab. Semua perubahan bakal di track.

**TL;DR : add, commit, pull, push**

Ketika versi 1 udh selesai, bakal merge dari development ke main. 

Bisa di rollback, baca documentation

## Conflict

Apapun yang ditambahin & sama. 

Kalau semisal pull dari development & ada conflict, langsung cek ke kodingan yang ada conflictnya.

Git ada "Auto correct" bisa pilih change dari sana, change dari kita, keduanya dan compare kedua changenya. 

Kebanyakan push bakal ada conflict. Beberapa butuh confirm ke orang lain buat handle conflictnya.



### Reverting / Resetting

Revert to selected commit

```
git log --oneline
git revert [commit_id] // softdelete, tersimpan di history
git reset [commit_id] // harddelete, gk tersimpan di history
```

Perhatikan revert & reset. 