# Day 003 - Design Pattern

Airplane bisa pake HashMap (Lebih efisien ?)

Di app biasanya kalau hanya punya sedikit currency proprietary, bakal merasa rugi & beli lagi / topup. 

**Biasakan buat diagram dulu sebelum buat app !!** , pake xmind / apapun



## Creating Java Project

Eclipse pake versi enterprise

1. Pilih project Name
2. Ada folder src
3. Buat package di src, namenya tergantung perusahaan
4. Buat package sesuai dengan kebutuhan (Biasanya Model View Controller)
5. Bisa buat getter setter otomatis



Setter getter inisialize di model

Setter getter di beri nilai di view



Setiap buat aplikasi pastikan punya blueprintnnya terlebih dahulu

Abstract tidak bisa di initiate, hanya sebagai blueprint saja. 



Proses dev app:

- Rancang dari map yang kita buat
- Buat blueprintnya, biar terstruktur
- Set data



Bagian main / frontend biasanya hanya menampilkan println atau sebagai input



Hierarchy di dalam java membantu kode lebih rapi / ramping dan bisa di reuse



Metehod yang repetitive / banyak dipakai taro di dalam class, dimana akan available untuk semuanya



Functional Programming, tidak di pakai di nexsoft. READ Lambda expression untuk javascript



Payment gateway biasa menggunakan midtrans (READ midtrans doc ?)



## Design Pattern

### Singleton 

satu instance class dibagi ke seluruh aplikasi

Cara penulisan :

- const private
- private static variable
- public static method untuk mengembalikan class



Semua object akan mengacu ke 1 alamat memory yang sama



Biasanya untuk database. tapi minusnya akan lama atau resource memory habis. Biasanya dibuat secukupnya 

Singleton bisa dibilang anti-pattern. Gunakan singleton seperlunya



### Encampulation object creation (Factory)

extends, extends, extends

ada kata Factory di belakang nama class. Ada "pabrik" yang membuat object

Biasa untuk database, misal mau connect ke client, tinggal "Buat" koneksi ke client dari if, connect ke database hrd, sisa if ke hrd_database



Return null. Wajib Null



### Decorator

Pola strucutre untuk menambahkan tanggung jawab baru. Cocok untuk tugas :

- caching

- hitung waktu proses method

- Acess Control system



### Template Method

Ada abstract class sebagai parent class, digunakan sebagai blueprint. Tapi ada beberapa metod yang berbeda untuk setiap childnya

Ada general method yang bisa dipakai berkali kali



Contoh pembayaran

Sama sama pesan, dapet id_pembayaran, nama barangnya apa, etc. Tapi ada type pembayaran yang berbeda dan deliverynya berbeda.



### Facade

subsystem yang dipakai. Misal mobil ada 1 kunci, yang bisa menyalakan macam macam system, misal AC, mesin, Sensor dll. per komponen itu subsystem. 1 facade mengatur banyak subsystem. 

Kasus nyatanya : Misal toko komputer, yang bisa rakit sendiri, bisa menggunakan facade, ada feature on off per dari processor / komponen. misal pilih AMD yang ditampilin mobo yang compatible. 

Lebih cocok untuk app yang preset bentuknya.




### Strategy

Meringkas code di class terpisah. Reusing code. 

Diapake ketika punya banyak logic yang menggunakan if-else

Class yang ditambah. 

Banyak dipakai untuk palikasi yang featurenya banyak dikembangkan, setiap minggu ada update



### Command

Memisahkan logic dengan consumer

Banyak dipakai di chat, home automation

command lebih spesifik mau nyalain class/ methode apa dibandingkan dengan facade yang 1 nyala semua nyala, 1 mati semua mati.



### Builder

Bisa dimodifikasi / dynamic.

Bisa diatur sesuai dengan kebutuhan app / user

 Builder boleh menggunakan innerclass



### Observer

Melihat ke 1 database, apakah berubah / tidak. 

Misal subscription youtube, 1 channel liat jumlah sub di database. 



### Abstract Factory

Lihat Slide



### HandsOn

Proses seorang penumpang di airport.

Ada ticket, pasport, luggage

Ada security pertama, jika salah satu tidak terpenuhi balik kecuali bawa ticket, passport tapi tidak bawa luggage.



akan cek xray, apakah aman atau tidak 



cek ke counter airline, di cek lagi ticketnya (tanggal / keaslian / counter), cek pasport apakah masih valid / tidak, minimal valid 6 bulan + waktu bertempat disana. Cek weight luggage apakah weightnya valid



lolos cek ke imigrasi, cek passport lagi dan ticket



Cek xray lagi



ke waiting room dan di cek lagi pasport , ticket dan hand luggage (tas bawaan, tidak boleh tralu besar, makanan / minuman) 



Masuk ke pesawat terbang liburan



Tugasnya buat app

Buat blueprint jangan lupa



### Formative

1. Tugas Xmind -> buat aplikasi ringan spt school yg isinya student, subject, dll. Kalau bisa berbeda masing-masing. Besok ditukar untuk dibuat codenya. Simpan di folder community masing-masing.

   Nama-Xmind-1.[extention]

4 code, 4 design, dengan contoh yang beda di masing masing codenya.







