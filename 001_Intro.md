# Day 001 Introduction

### Build Tools

Mengotomatiskan proses pembuatan aplikasi

Tugas :

- downloading && adding dependencies
  - Dependencies yang dipakai otomatis di download
- Compile ke bytecode
- packaging compiled code
- running test
  - Yang paling benar test driven development, ada test case lebih dahulu
- Deploy ke production env
  - CI/CD, bantu deploy ke env 1 ke env lainnya



### Java-based build toos

Apache Ant, Apache Maven, Gradle

##### Apache Ant

buat aplikasi yang pake CLI, masih digunakan tapi udh gk keliatan

##### Apache Maven

Lebih advance. Convention over Configuartion, jadi dev hanya perlu menentukan aspek aplikasi yang tidak konvensional (Dev yang tentuin sendiri dependency yang diperlukan saja)



### Apache Maven Basic

Maven atur package otomatis sampe ke versinya. Download dari remote repo ke local machine, kalau butuh framework yang sama di project beda, gk perlu donwload lagi



### Project

GroupID : Biasanya mengikuti perusahaan, misal id.co.nexsoft.[nama_app]

artifactId : Nama project yang dibuat, misal : first-app

version : versioning, 1.0, 2.0, dst



### Maven Command

```
mvn archetype:generate "-bla bla bla" "-bla bla bla"
```

Folder project dibuat otomatis, folder utama di main & test. Awalnya lebih baik dibuat di **AppTest.java** 

Buat kerangka coding, testing feature baru, dll di AppTest.java



### Build Result

.jar, .war, .java

.java : bisa execute di CLI, tapi harus include packaging

.war : bisa komunikasi ke server, diakses ke HTTP

.jar : 

```
java -cp target/app-name-1.jar package.name.App
```



```
mvn clean
```

Untuk hapus hasil build terkahir dari project. Untuk menghindari kesalahan versi.



### Gradle Basic

Mirip maven, build tools java, atur segala dependencies, repo di Java

Bedanya dengan maven, stylingnya beda, lebih banyak dipake di mobile dev, beda syntax (maven xml, gradle mirip json)



Ketika masuk ke dalam team, lihat buildtools apa, IDE apa. Kalau bisa disamain, biar troubleshooting gampang / seragam. 



## IDE (Integrated Dev Env)

Eclipse, netbeans, intellij

Recommendnya : Install semuanya. 



Sebagai Text Editor, Translator (compiler / interpreter), build automation tools, debugger

Pastikan setting IDE seragam, padding / spacing sama. Biar ketika merge code, tidak ada bentrok.

Breakpoint for debuggings



Netbeans untuk enterprise app, semua library yang digunakan dipilih sendiri versinya. 

Sekarang boleh menggunakan IDE di bootcamp. 





## Handson 001

Ada person, pengecekkan, complete. 



Pengumpulan ada 2 git

ada community git & pribadi

pribadi seperti biasa formative & summative. Nama project, extention, etc ditentukan. 

community untuk belajar. 



Penggunaan if statement biasanya ada urutan (lebih ke javascript)

Scanner juga ada urutannya

Performance check juga perlu, waktu proses & compile time etc



## Command Line

CLI (Command Line Interface)

Pake command line biar lebih flexible dan banyak option ketika ingin melakukan sesuatu. 

kalau dari UI terkadang dibatasi, mungkin sisanyfa harus bayar / melewati menu yang beda. 



Server pake linux biar free license



## HTTP Client

RESTful API. Pake appnya insomnia / Postman

GET data ada di parameter

POST data ada di body / packet, jadi data yang dikirim encrypted (Password & email wajib POST)



## Algorithm

Class Array punya algo untuk manipulasi array & searching

Salah satunya findIndexOfMax(), buat cari nilai tertinggi di array yang nilainya tidak harus berurutan. 



Corner Case -> perilaku salah yang salah atau ambigu. Misal 1+a=?. Usahakan buat semua output kemungkinan, biar tidak ada yang ambigu ketika test. 

Misal 

```
findIndexOfMax(new int[] {}); // hasil index 0, karena tidak ada angka di array

if(numbers.length == 0){
	return -1; // sreturn -1 biar tidak gantung outputnya
}
```

Ketika membuat app pikirkan apakah ada sesuatu yang bisa membuat aplikasi gagal. 



### Linear Search

Searching yang dari awal ke akhir. Kurang efisien ketika banyak data. 



### Sorted Array in Java

di sort, kemudian di search. Kurang efisien juga



### Jump search java

lompat-lompat untuk melakukan searching. Akan ada blocking ketika ingin melakukan search. Semakin lama semakin kecil. Efisien tapi kompleks.



### Bubble sort Java

Akan dibawa ke atas. data diambil & dibawa keatas



### Qucik Sort Java

Lihat kodingan, left right search. Lebih sering digunakan di frontend. 



### String Algo

containsPattern : absolute search / regex

Corner Casing : containsPatter("", "") hasil bisa true



### Knuth-Morris-Pratt algo in Java

KMP perbandingan symbol demi symbol dengan substring text. 

Bisa untuk searching seberapa occurence string yang terjadi di sebuah string. 

Bisa untuk full text search



Biasanya data ada di local, karena kalau server-client sambung, aplikasi akan lambat. Filter, searching dan sorting semuanya ada di frontend. 



### Structure Tree

Grafik dengan kondisi tertentu, tanpa siklus. 

Terminology :

- root node : node paling atas dari sebuah tree
- leaf : node yang tanpa anak
- parent : node yang punya anak
- etc..



