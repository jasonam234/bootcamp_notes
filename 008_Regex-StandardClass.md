# 008 - Standard Class & Regex

arrays sort bisa sorting array, tapi hanya yang primitive. 

## Math Lib

Yang sering dipake :

int min = Math.min(11, 81);

int max = Math.max(11, 81);

int abs = Math.abs(-10); // minus jadi plus.

int floor = Math.floor(3.78); //pembulatan bawah return 3

int ceiling = Math.ceil(4.15); //pembulatan atas return 5

Math.sqrt = squarerooot

Math.cbrt = akar pangkat 3

Math.pow = pangkat dari argumen 1. Nilai pangkatnya dari argumen 2

Math.sin & Math.cos = balikin radian dari sin / cos

Math.random() = range dari 0 - 1



```
RNG 0/1

public static void main(){
	double rand = Math.round(Math.random());
	printf(rand);
}
```

Math lib bisa digabungin , funct dalam funct



## Random

```
import java.util.random;

public static void main(){
	Random random = new Random(100);
	System.out.println(random.nextInt(10)); // 0 - 9
	
}
```

nextInt ada beberapa variant, nextLong, nextInt, nextDouble, etc

nextDouble range hanya dari 0.0 - 1.0 dengan decimal yang banyak



### Handson

1. Buat method urutkanAbjad(str), str nanti di sort dari a - z

   misal hello jadi ehllo

   inputnya bisa huruf besar.

2. method shopping Time(memberId, money)

   Kruang dari sama dengan 50 ribu, maka print uang tidak cukup



## BigInt

Misal butuh angkat yang lebih dari int, pake BigInt



## Time

Local Date Format YYYY-MM-dd

Library ada 3 : Local time, Local date, local datetime

Tergantung penggunaan nanti.



Bikin app biasanya pake timestamp



Parsing tanggal gk bisa pake string



data.plusDays(5) -> tambahin tanggal. 



## RegEx

Manipulasi data text, lebih flexible ? 



```
System.out.println("ale".matches("ale")); // true
System.out.println("ALE".matches("ale")); // false
```

buat pattern ada symbol / gk



## Formative

Format NIK harus NIK-00-{bebas}, kalau tidak error

jam kedatangan diinput

Jam 8.00 pagi harus masuk, ketika lebih dari 15 menit, maka app akan kasih info perinagatan.

Formatting pake regex, NIK bisa input / otomatis tambah



Misal 8.05, waktu keterlambatan tetap 0, karena toleransi 15 menit





Huruf O besar yang ada di formative.

Netflix langsung buat printf 1 thaun kedepan. 



