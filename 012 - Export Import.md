# 012 - Export Import etc

### Accessing folder / directory traversing

```
../ -> keluar 1x
../../ -> keluar 2x
../../aaa -> keluar 2x & access folder aaa
./ -> current working dir
```



### Import - Export

```
import {printName, printAge} from "./utility";

console.log(printName("Budi"));

{printName, printAge} -> Destructuring
```



```
const printName = (name) => {
	return "name : " + name;
}

const printAge = (age) => {
	return "age : " + age;
}

export {printName as default, printAge}
printName nanti tetap dijalanin tanpa harus destructuring

//creating alias for name
export {printName as cetakNama}
import {cetakName} from "./utility.js" -> di main.js ketik cetakName, bukan printName

import * as PrintFunction form "./utility" -> import semua yang ada di i=utility
PrintFunction.printName("aaa");
```

Semua bisa di export, termasuk variable, function, etc

Kalau ada function yang dipake berulang di beberapa file, buat di utility.js & export ke masing-masing file. Code jadi lebih efisien



### Spread Operator

array spreading

```
const numbers = [1,2,3];
const numbers2 = numbers;

numbers2[2] = 1000;

//Sama karena bagi memory address yang sama
console.log(numbers = [1,2,1000])
console.log(numbers2 = [1,2,1000])

//Using spread operator
const numbers = [1,2,3];
const numbers2 = [...numbers]; //numbers 2 assigned to new memory address

numbers2[2] = 1000;

//Sama karena bagi memory address yang sama
console.log(numbers = [1,2,1000])
console.log(numbers2 = [1,2,1000])
```



Object spreading

```
const person = {
	name: "budi",
    address : "jakarta",
    age : 20
};

const newPerson = {
	...person,
    address: "tangeran",
	age: 26,
}

console.log(newPerson);

output -> budi, tangeran, 26
Key lama bakal di replace dengan yang baru
```

Spread biar object yang mutable jadi immutable



### Destructuring

array destruct

```javascript
const numbers = [1, 2, 3];
[num1, num2, num3] = numbers;

console.log(num1);
console.log(num2);
console.log(num3);
```

Object destruct

````javascript
const hero = {
    name : "batman",
    realName : "bruce wayne",
    address : {
    	city : "Gotham"
	}
}
const {name : superHero , realName : namaJagoan, address: {city:kota}} = hero
console.log(superHero);
console.log(namaJagoan);
console.log(kota);
````



### Map, Reduce & Filter

```javascript
function ganjil(...args){
    return args.map((num) => { return num % 2 !== 0});
}//return array of boolean, kalau tidak return undefined

//num = item yang di iterasi ke x
//jangan lupa return, return 1 untuk function utama, return 2 untuk booleannya

function genap(...args){
    return args.filter((num) => {return num % 2 === 0});
}//return sebuah array yang sesuai dengan kondisi, yang ganjil tidak masuk, kalau tidak return []

function sumNumber(...args){
    return args.reduce((prev, current) => {return prev + current});
}//balikin string yang sudah diberi kondisi. ada 2 param, prev & current, reduce otomatis bava prev & current, kalau tidak return undefined
```

Map, filter & reduce digunain buat memory optimisation, lebih ringan dibandingin looping biasa.



### Async

Sync -> berurutan, di js sendiri sync

```
code 1
code 2
code 3
```

Async -> Pake Async wait



### Summative

Soal 1

Summative besok : Mini Game

Nama kelompok :

Tema : 



HTML CSS JAVASCRIPT

